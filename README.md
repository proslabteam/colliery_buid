# README #

This is repository contains the jar build of the Colliery tool, and the script files for using the Pm4Py libraries.

##Requirements

* Java Runtime Environment 
* Python v. 3.x
* [Pm4Py](https://pm4py.fit.fraunhofer.de/install)

## Usage

Make sure to have downloaded in the same directory both the Colliery jar file and the scripts folder. Then execute the jar file with double click or from terminal.
 
Example:

```bash
java -jar Colliery.jar  
```
 
